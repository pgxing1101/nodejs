// MongoDB是一个基于分布式文件存储的文档型数据库;
// MongoDB是一个介于关系数据库和非关系数据库之间的产品。
// Mongoose是在node.js异步环境下对MongoDB进行便捷操作的对象模型工具;
// Mongoose是针对MongoDB操作的一个对象模型库，封装了MongoDB对文档的、增删改查等方法。
// 1、安装 cnpm install mongoose --save
// 2、引入并连接数据库
const mongoose = require("mongoose")
mongoose.connect("mongodb://127.0.0.1:27017/main");
// 如果有账户密码
// mongoose.connect("mongodb://user:123456@127.0.0.1:27017/main")
// userNewUrlParser这个属性会是被验证用户所需的db
// mongoose.connect("", { userNewUrlParser: true })
// 3、定义schema
/*数据库中的Schema，为数据库对象的集合。
schema是mongoose里会用到的一种数据模式，
可以理解为表结构的定义；
每个shcema会映射到mongodb中的一个collection,
它不具备操作数据库的能力。 
（相当于TS中的类型定义）*/
let UserSchema = mongoose.Schema({
    _id: Number,
    name: String,
    username: String,
    email: String,
    address: {
        street: String,
        suite: String,
        city: String,
        zipcode: String,
        geo: {
            lat: String,
            lng: String
        }
    },
    phone: String,
    website: String,
    company: {
        name: String,
        catchPhrase: String,
        bs: String
    }
    // other:{type:String,default:1} // 可以指定默认参数
})
// 4、创建数据模型
// mongoose.model(参数1：模型名称（首字母大写）,参数2：Schema，参数3：数据库集合名称)
let User = mongoose.model('User', UserSchema, 'userdata')
// User.find(
//     {},//条件
//     (err, result) => {
//         if (err) return console.log(err)
//         console.log(result[0])
//     }
// )
// 5、增加数据
// 实例化模型
// let user = new User({
//     _id: 100,
//     name: 'test',
//     username: 'test',
//     email: 'test',
//     address: {
//         street: 'test',
//         suite: 'test',
//         city: 'test',
//         zipcode: 'test',
//         geo: {
//             lat: 'test',
//             lng: 'test'
//         }
//     },
//     phone: 'test',
//     website: 'test',
//     company: {
//         name: 'test',
//         catchPhrase: 'test',
//         bs: 'test'
//     }
// })
// console.log(user)
// user.save(function (err) {
//     if (err) return console.log(err)
//     console.log('增加数据成功')
// })
// 6、修改数据
// User.updateOne(
//     { _id: 100 },//条件
//     { name: 'test100' }, // 更改的内容
//     (err) => {
//         if (err) return console.log(err)
//         console.log('修改数据成功')
//     }
// )
// 7、查找数据
User.find(
    {  },
    (err, result) => {
        if (err) return console.log(err)
        console.log(result)
    }
)
// 8、删除数据
// User.deleteOne(
//     { name: 'test100' },
//     (err) => {
//         if (err) return console.log(err)
//         console.log('删除数据成功')
//     }
// )
// 更多详细教程：http://mongoosejs.net/

