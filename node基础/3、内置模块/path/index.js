const path = require("path");
// 1、path.join() unix系统是"/"，windows系统是"\"
var url = path.join('123', '456', 'aaa');
console.log(url) // 123\456\aaa
// 2、basename返回文件名称，如果加第二个参数，则返回的结果会删除该后缀
var basename = path.basename('www.baidu.com/aaa/bbb/ccc/pgx.html', '.html')
console.log(basename) // pgx
// 3、dirname(path) 返回文件的目录名称
var dirname = path.dirname('./aaa/bbb/ccc/111.txt')
console.log(dirname) // ./aaa/bbb/ccc