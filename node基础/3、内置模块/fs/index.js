const fs = require("fs")
// 1、得到文件与目录的信息:stat
fs.stat('./1.txt', (err, stats) => {
    console.log(stats.isFile()) //true
})
// 2、创建一个目录：mkdir
fs.mkdir('./createDir', err => {
    if (err) {
        console.log(err)
    }
})
// 3、创建文件并写入内容：writeFile
fs.writeFile('./2.txt', '文件2', (err) => {
    if (err) {
        console.log(err)
    }
})
// 4、读取文件的内容：readFile readFileSync
fs.readFile('./2.txt', 'utf-8', (err, stats) => {
    if (err) return console.log(err)
    console.log(stats)
})
// 5、列出目录的东西：readdir
fs.readdir('./', 'utf-8', (err, list) => {
    if (err) return console.log(err)
    console.log(list)
})
// 6、重命名目录与文件：rename
fs.rename('./1.txt', 'new.txt', err => {
    if (err) return console.log(err)
})