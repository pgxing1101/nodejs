// 相关配置
let server = (require, response) => {
    // 发送http头部
    response.writeHead(200, { 'content-type': 'text/html;charset=utf-8' })
    // 返回的内容
    response.write('请求完毕')
    // 关闭
    response.end()
}
// 暴露接口
module.exports = server