// 模块化（定义、暴露接口、调用）
const http = require('http')
const baseServer = require('./baseServer')
let server = http.createServer(baseServer)
// 设置端口
server.listen(8888)
console.log('server running at http://127.0.0.1:8888')
