var url = require('url')
// 1、parse 将url的字符串解析并返回一个url对象，第二个参数默认false，为true时，query返回对象。
var urlParse = url.parse('www.baidu.com:8888/aaa?a=111', true)
console.log('urlParse', urlParse)
// 结果：
// {
//     protocol: 'www.baidu.com:',
//     slashes: null,
//     auth: null,
//     host: '8888',
//     port: null,
//     hostname: '8888',
//     hash: null,
//     search: '?a=111',
//     query: [Object: null prototype] { a: '111' },
//     pathname: '/aaa',
//     path: '/aaa?a=111',
//     href: 'www.baidu.com:8888/aaa?a=111'
// }

// 2、format 将传入的url对象变成一个url字符串并返回
var urlString = url.format(urlParse)
console.log('urlString', urlString) // www.baidu.com:8888/aaa?a=111

// 3、resolve 替换域名后面最后一个"/"后的内容
var a = url.resolve('/one/two/three', 'four'),
    b = url.resolve('http://example.com', '/one'),
    c = url.resolve('http://example.com/one', '/two')
console.log(a + ',' + b + ',' + c)
// /one/two/four,http://example.com/one,http://example.com/two