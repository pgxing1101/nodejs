// Express基于 Node.js 平台，快速、开放、极简的 Web 开发框架
const express = require("express")
const app = express()
app.get('/', (req, res) => res.send('Hello World!'))
app.listen(8888, () => console.log('Example app listening on port 3000'))
// express路由配置
// let router = express.Router()
// // 该路由使用的中间件
// router.use((req,res,next)=>{
//     next()
// })
// // 定义网站主页的路由
// router.post('/addData',function(req,res){
//     console.log('addData')

// })
app.post('/add', function (req, res) {
    res.send('Got a POST request')
})
// 静态资源
app.use(express.static('public'))