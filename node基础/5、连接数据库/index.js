// 1、cnpm install mongodb --save
// 2、引入mongodb
const { MongoClient } = require('mongodb')
// 3、定义数据库连接的地址
const url = 'mongodb://localhost:27017'
// 4、定义要操作的数据库
const dbName = 'main'
// 5、实例化MongoClient 传入数据库连接地址
const client = new MongoClient(url, { useUnifiedTopology: true })
// 6、连接数据库 操作数据
client.connect(function (err) {
    if (err) return console.log(err)
    console.log("数据库连接成功")
    let db = client.db(dbName);
    // 1、查找数据
    db.collection("userdata").find({ "_id": 11 }).toArray((err, data) => {
        if (err) {
            console.log(err)
            return
        }
        console.log(data)
        // 操作数据库完毕以后一定要 关闭数据库连接
        client.close()
    })
    // 2、增加数据
    // db.collection("userdata").insertOne({ "_id": 12, "name": "pgx", "username": 'pgx' }, (err, result) => {
    //     // 增加失败
    //     if (err) return console.log(err)
    //     console.log('增加成功')
    //     console.log(result)
    //     client.close()
    // })
    // 3、修改数据
    // db.collection("userdata").updateOne({"_id":11},{$set:{"name":"kdy"}},(err,result)=>{
    //     if(err) return console.log(err)
    //     console.log("增加成功")
    //     console.log(result)
    //     client.close()
    // })
    // 4、删除一条数据
    // db.collection("userdata").deleteOne({"name":'kdy'},(err)=>{
    //     if(err) return console.log(err)
    //     console.log("删除一条数据成功")
    //     client.close()
    // })
    // 5、删除多条数据
    // db.collection("userdata").deleteMany({"name":'pgx'},err=>{
    //     if(err) return console.log(err)
    //     console.log("删除多条数据成功！")
    //     client.close()
    // })
})
