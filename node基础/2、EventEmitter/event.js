// events模块只提供了一个对象：event.EventEmitter。EventEmitter的核心就是事件触发与事件监听器功能的封装。
var events = require("events")
var event = new events.EventEmitter()
event.on('pgx',function(){
    console.log('pgx')
})
setTimeout(()=>{
    event.emit('pgx')
},3000)
