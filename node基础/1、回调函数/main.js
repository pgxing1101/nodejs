var fs = require("fs")
// A：阻塞代码实例
var data = fs.readFileSync('input.txt')
console.log('A：' + data.toString());

// B：非阻塞代码实例-异步
fs.readFile('input.txt', function (err, data) {
    if (err) return console.error(err)
    console.log('B：' + data.toString())
})

console.log("程序执行结束！")

// 结果：
// A：hello world!
// 程序执行结束！
// B：hello world!