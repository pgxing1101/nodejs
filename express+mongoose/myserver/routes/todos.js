const express = require('express')
const router = express.Router()
const Todos = require('./../mongoose/model/todos')
router.get('/', (req, res, next) => {
    const { pageNum, pageSize } = req.query
    if (pageNum && pageSize) {
        Todos.count({}, (err, count) => {
            // 超出总数量
            if (((pageNum - 1) * pageSize + 1) >= count) {
                res.send({
                    code: 0,
                    desc: '超出总数',
                    data: {}
                })
            } else {
                Todos.find({})
                    .skip((pageNum - 1) * pageSize + 1).limit(pageSize)
                    .then(result => {
                        res.send({
                            code: 1,
                            desc: '查询成功',
                            data: {
                                pageNum,
                                pageSize,
                                count,
                                result
                            }
                        })
                    })
                    .catch(err => {
                        console.log(err)
                        return false
                    })
            }
        })
    } else {
        Todos.find({}, (err, result) => {
            res.send({
                code: 1,
                desc: '查询成功',
                data: {
                    pageNum: '',
                    pageSize: '',
                    count: '',
                    result
                }
            })
        })
    }


})
module.exports = router