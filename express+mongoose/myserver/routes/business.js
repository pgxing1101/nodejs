const express = require('express')
const router = express.Router()
const Business = require('./../mongoose/model/business')
router.get('/', (req, res) => {
    Business.find({}, (err, result) => {
        if (err) return console.log(err)
        res.send(result)
    })
})
module.exports = router