const express = require('express');
const router = express.Router();
const User = require('./../mongoose/model/user')
/* GET users listing. */
// 查询用户
router.get('/', function (req, res, next) {
  User.find(req.query, (err, result) => {
    if (err) return console.log(err)
    res.send(result)
  })
});
// 新增用户
router.post('/add', function (req, res, next) {
  let user = new User(req.body)
  user.save(function (err) {
    if (err) return console.log(err)
    res.send('新增用户成功')
  })
})
// 修改用户
router.post('/update', function (req, res) {
  User.updateOne({ _id: req.body._id }, req.body, err => {
    if (err) return console.log(err)
    res.send('用户修改成功')
  })
})
// 删除用户
router.post('/delete', function (req, res) {
  User.deleteOne({ _id: req.body._id }, function (err) {
    if (err) return console.log(err)
    res.send('删除成功！')
  })
})

module.exports = router;
