const express = require('express')
const router = express.Router()
const Account = require('./../mongoose/model/account')
// 注册
router.post('/register', (req, res, next) => {
    // 查询要注册的用户名是否已存在
    Account.find({ account: req.body.account }, (err, result) => {
        if (err) return console.log(err)
        let user = result[0]
        // 用户已存在，不能重复注册
        if (user) {
            res.send({
                code: 0,
                message: '用户名已存在！'
            })
        } else {
            const account = new Account(req.body)
            account.save((err) => {
                if (err) return console.log(err)
                res.send({
                    code: 1,
                    message: '注册成功！'
                })
            })
        }
    })
})
// 登录
router.post('/login', (req, res, next) => {
    // 根据account查询用户是否存在
    Account.find({ account: req.body.account }, (err, result) => {
        if (err) return console.log(err)
        let user = result[0]
        if (user) {
            // 校验密码
            if (user.password === req.body.password) {
                res.send({
                    code: 1,
                    message: '登录成功！'
                })
            } else {
                res.send({
                    code: 2,
                    message: '密码不正确！'
                })
            }
        } else {
            res.send({
                code: 3,
                message: '用户不存在！'
            })
        }
    })
})
module.exports = router