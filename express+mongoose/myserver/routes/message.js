const express = require('express');
const router = express.Router();
// 1、导入message（数据库表）的model。
const Message = require('./../mongoose/model/message');
/* GET users listing. */
// 查询用户
router.get('/', function (req, res, next) {
  // 2、使用model内置方法find查询表内数据
  const { pageNum, pageSize } = req.query
  if (pageNum && pageSize) {
    Message.count({}, (err, count) => {
      // 超出总数量
      if (((pageNum - 1) * pageSize + 1) >= count) {
        res.send({
          code: 0,
          desc: '超出总数',
          data: {
            result:[]
          }
        })
      } else {
        Message.find({})
          .skip((pageNum - 1) * pageSize + 1).limit(pageSize)
          .then(result => {
            res.send({
              code: 1,
              desc: '查询成功',
              data: {
                pageNum,
                pageSize,
                count,
                result
              }
            })
          })
          .catch(err => {
            console.log(err)
            return false
          })
      }
    })
  } else {
    Message.find({}, (err, result) => {
      if (err) return console.log(err)
      res.send({
        code: 1,
        msg: '查询成功',
        data: {
          pageNum: '',
          pageSize: '',
          count: '',
          result
        }
      })
    })
  }
  // Message.find(req.query, (err, result) => {
  //   if (err) return console.log(err)
  //   res.send(result)
  // })
});
router.get('/:id', function (req, res, next) {
  Message.find(req.params, (err, result) => {
    if (err) return console.log(err)
    res.send(result[0])
  })
});

module.exports = router;
