// 1、导入mongoDB数据库连接方法，建立连接
const mongoose = require('./../connect');
// 2、定义schema
const MessageSchema = mongoose.Schema({
    id: Number,
    userId: Number,
    title: String,
    body: String
})
// 3、导出model（直接操作数据库，增删改查）
module.exports = mongoose.model('Message', MessageSchema, 'message')