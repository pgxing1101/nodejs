const mongoose = require('./../connect')
const AccountSchema = mongoose.Schema({
    account: String,
    password: String
})
module.exports = mongoose.model('account', AccountSchema, 'account')