const mongoose = require('./../connect')
const BusinessSchema = mongoose.Schema({
    column: String,
    children: []
})
module.exports = mongoose.model('Business', BusinessSchema, 'Business')