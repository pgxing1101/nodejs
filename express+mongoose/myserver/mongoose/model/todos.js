const mongoose = require('./../connect')
const TodosSchema = mongoose.Schema({
    userId: Number,
    title: String,
    completed: Boolean
})
module.exports = mongoose.model('Todos', TodosSchema, 'todos')