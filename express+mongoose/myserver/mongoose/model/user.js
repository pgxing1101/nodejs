const mongoose = require('./../connect')
const UserSchema = mongoose.Schema({
    username: {
        type: String,
        default: ""
    },
    email: {
        type: String,
        default: ""
    },
    phone: {
        type: String,
        default: ""
    },
    company: {
        type: String,
        default: ""
    },
    address: {
        street: {
            type: String,
            default: ""
        },
        city: {
            type: String,
            default: ""
        }
    }
    // other:{type:String,default:1} // 可以指定默认参数
})
module.exports = mongoose.model('User', UserSchema, 'userdata')
