// 使用nodejs爬取豆瓣电影top250
const https = require('https')
const cheerio = require('cheerio')//需安装 npm install cheerio
const fs = require('fs')
https.get('https://movie.douban.com/top250', function (res) {
    let html = ''
    res.on('data', function (chunk) {
        html += chunk
    })
    res.on('end', function () {
        const $ = cheerio.load(html)
        let allFilms = []
        $('li .item').each(function () {
            const title = $('.title', this).text()
            const star = $('.rating_num', this).text()
            const pic = $('.pic img', this).attr('src')
            allFilms.push({
                title,
                star,
                pic
            })
        })
        fs.writeFile('./films.json', JSON.stringify(allFilms), function (err) {
            if (!err) {
                console.log('文件写入完毕')
            }
            //下载图片
            downloadImages(allFilms)
        })
    })
})
function downloadImages(allFilms) {
    for (let i = 0; i < allFilms.length; i++) {
        const picUrl = allFilms[i].pic
        https.get(picUrl, function (res) {
            res.setEncoding('binary')
            let str = ''
            res.on('data', function (chunk) {
                str += chunk
            })
            res.on('end', function () {
                //images文件夹需要提前创建
                fs.writeFile(`./images/${i}.png`, str, 'binary', function (err) {
                    if (!err) {
                        console.log(`第${i}张图片下载成功`)
                    }
                })
            })
        })
    }
}
